import org.example.service.ModelData;
import org.example.service.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OperationTest {
    Operation operation;

    @BeforeEach
    void initiate(){
        operation = new Operation();
        Map<String, ModelData> expectedData = new HashMap<>();
    }

    @Test
    void readFile_FileExist(){
        String path = "/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummy.csv";
        int expected = 0;
        int result = operation.readFile(path);
        // status code
        Assertions.assertEquals(expected,result);
        Map<String, ModelData> expectedData = new HashMap<>();
        ModelData modelData = new ModelData();
        ModelData modelData2 = new ModelData();
        int [] dataNilai = {7};
        modelData.setKelas("dummy");
        modelData.setNilai(dataNilai);
        expectedData.put("dummy", modelData);
        modelData2.setKelas("SemuaKelas");
        modelData2.setNilai(dataNilai);
        expectedData.put("SemuaKelas", modelData2);
        // actual data
        Assertions.assertEquals(expectedData,operation.dataMap);
    }

    @Test
    void readFile_FileNotExistCase(){
        String path = "zzzzzzzzzzzz";
        int expected = 1;
        int result = operation.readFile(path);
        Assertions.assertEquals(expected,result);
        Map<String, ModelData> expectedData = new HashMap<>();
        Assertions.assertEquals(expectedData,operation.dataMap);
    }

    @Test
    void readFile_null(){
        String path = null;
        int expected = 1;
        int result = operation.readFile(path);
        Assertions.assertEquals(expected,result);
        Map<String, ModelData> expectedData = new HashMap<>();
        Assertions.assertEquals(expectedData,operation.dataMap);
    }

    @Test
    void writeFile_success() throws IOException {
        String path = "/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummy.csv";
        int expected = 0;
        operation.readFile(path);
        int result = operation.write("/home/robert/IdeaProjects/Challenge_Chapter3/src/test/result1.txt");
        Assertions.assertEquals(expected,result);

        Path path1 = Paths.get("/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummyResult1.txt");
        Path path2 = Paths.get("/home/robert/IdeaProjects/Challenge_Chapter3/src/test/result1.txt");
        List<String> expectedFile = Files.readAllLines(path1);
        List<String> actualFile = Files.readAllLines(path2);

        Assertions.assertEquals(expectedFile.size(), actualFile.size());

        for(int i = 0; i < expectedFile.size(); i++) {
            System.out.println("Perbandingan baris: " + i);
            Assertions.assertEquals(expectedFile.get(i), actualFile.get(i));
        }
    }

    @Test
    void writeFile_empty() throws IOException {
        String path = "/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummy.csv";
        int expected = 1;
        operation.readFile(path);
        int result = operation.write("");
        Assertions.assertEquals(expected,result);
    }

    @Test
    void writeFile_null() throws IOException {
        String path = "/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummy.csv";
        int expected = 1;
        operation.readFile(path);
        int result = operation.write(null);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void writeFile2_success() throws IOException {
        String path = "/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummy.csv";
        int expected = 0;
        operation.readFile(path);
        int result = operation.write2("/home/robert/IdeaProjects/Challenge_Chapter3/src/test/result2.txt");
        Assertions.assertEquals(expected,result);

        Path path1 = Paths.get("/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummyResult2.txt");
        Path path2 = Paths.get("/home/robert/IdeaProjects/Challenge_Chapter3/src/test/result2.txt");
        List<String> expectedFile = Files.readAllLines(path1);
        List<String> actualFile = Files.readAllLines(path2);

        Assertions.assertEquals(expectedFile.size(), actualFile.size());

        for(int i = 0; i < expectedFile.size(); i++) {
            System.out.println("Perbandingan baris: " + i);
            Assertions.assertEquals(expectedFile.get(i), actualFile.get(i));
        }
    }

    @Test
    void writeFile2_empty() throws IOException {
        String path = "/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummy.csv";
        int expected = 1;
        operation.readFile(path);
        int result = operation.write2("");
        Assertions.assertEquals(expected,result);
    }

    @Test
    void writeFile2_null() throws IOException {
        String path = "/home/robert/IdeaProjects/Challenge_Chapter3/src/test/dummy.csv";
        int expected = 1;
        operation.readFile(path);
        int result = operation.write2(null);
        Assertions.assertEquals(expected,result);
    }

}
