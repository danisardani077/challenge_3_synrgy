import org.example.service.Calculation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculationTest {
    Calculation calculation;

    @BeforeEach
    void initiate(){
        calculation = new Calculation();
    }

    @Test
    void rata2_normalCase(){
        int[] data = {7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10};
        double expected = 8.391304347826088;
        double result = calculation.rata2(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void rata2_bigDataCase(){
        int[] data = {7,7,7,7,7,7,7,8,8,8,8,1,9,9,9,9,9,9,10,10,10,10,10,7,7,7,7,7,7,7,8,8,8,3,8,9,9,9,9,9,9,10,10,10,10,10,4,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10};
        double expected = 8.173913043478262;
        double result = calculation.rata2(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void rata2_zeroCase(){
        int[] data = {0};
        double expected = 0;
        double result = calculation.rata2(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void rata2_negativeCase(){
        int[] data = {-1,-2,3,4,-2};
        double expected = 0.4;
        double result = calculation.rata2(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void rata2_nullCase(){
        int[] data = null;
        double expected = 0.0;
        double result = calculation.rata2(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void rata2_noDataCase(){
        int[] data = {};
        double expected = Double.NaN;
        double result = calculation.rata2(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void nilaiTengah_normalCase(){
        int[] data = {7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10};
        double expected = 8.0;
        double result = calculation.nilaiTengah(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void nilaiTengah_bigDataCase(){
        int[] data = {7,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9,3,7,2,7,1,9,9};
        double expected = 7.0;
        double result = calculation.nilaiTengah(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void nilaiTengah_zeroCase(){
        int[] data = {0};
        double expected = 0;
        double result = calculation.nilaiTengah(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void nilaiTengah_negativeCase(){
        int[] data = {-1,-2,-3};
        double expected = -2.0;
        double result = calculation.nilaiTengah(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void nilaiTengah_nullCase(){
        int[] data = null;
        double expected = 0.0;
        double result = calculation.nilaiTengah(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void nilaiTengah_noDataCase(){
        int[] data = {};
        double expected = 0.0;
        double result = calculation.nilaiTengah(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void modus_normalCase(){
        int[] data = {7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10};
        double expected = 7.0;
        double result = calculation.modus(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void modus_bigDataCase(){
        int[] data = {7,7,7,7,7,7,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,10,10,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10,7,8,8,8,8,8,9,9,9,9,9,9,10,10,10};
        double expected = 9.0;
        double result = calculation.modus(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void modus_zeroCase(){
        int[] data = {0};
        double expected = 0;
        double result = calculation.modus(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void modus_negativeCase(){
        int[] data = {-1,-2,-3,-3,-3,-3,-2,-3,-3};
        double expected = -3.0;
        double result = calculation.modus(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void modus_nullCase(){
        int[] data = null;
        double expected = 0.0;
        double result = calculation.modus(data);
        Assertions.assertEquals(expected,result);
    }

    @Test
    void modus_noDataCase(){
        int[] data = {};
        double expected = 0.0;
        double result = calculation.modus(data);
        Assertions.assertEquals(expected,result);
    }

}
