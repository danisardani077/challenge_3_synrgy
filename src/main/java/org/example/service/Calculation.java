package org.example.service;

import java.util.Arrays;

public class Calculation implements ICalculation{
    public double rata2(int[] data){
        double rata2 = 0;
        try {
        double total = Arrays.stream(data).sum();
        return total/data.length;
        } catch (NullPointerException nullPointer){
            System.out.println("input tidak boleh kosong");
            return rata2;
        }

    }
    public double nilaiTengah(int[] data){
        double nilaiTengah = 0;

        try {
            Arrays.sort(data);
            int panjangData = data.length;
            if ((panjangData % 2) == 0){
                nilaiTengah = (data[data.length/2] + data[data.length/2 - 1])/2;
            } else {
                nilaiTengah = data[data.length/2];
            }
            return nilaiTengah;
        } catch (NullPointerException nullPointer){
            System.out.println("input tidak boleh kosong");
            return nilaiTengah;
        } catch(ArrayIndexOutOfBoundsException e) {
            System.out.println("Data tidak boleh kosong");
            return nilaiTengah;
        }
    }
    public double modus(int[] data){
        int popularity1 = 0;
        int popularity2 = 0;
        int popularity_item = 0;
        int array_item = 0;
        try {
            for (int i =0;i<data.length;i++){
                array_item = data[i];
                for (int j =0;j<data.length;j++){
                    if(array_item == data[j]) {
                        popularity1++;
                    } else if (popularity1 >= popularity2) {
                        popularity_item = array_item;
                        popularity2 = popularity1;
                    }
                }
                popularity1 = 0;
            }
            return popularity_item;
        } catch (NullPointerException nullPointer){
            System.out.println("input tidak boleh kosong");
            return popularity_item;
        }
    }
}
