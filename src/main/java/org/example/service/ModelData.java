package org.example.service;

import lombok.Data;

@Data
public class ModelData {
    private int[] nilai;
    private String kelas;
}
