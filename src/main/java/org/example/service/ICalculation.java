package org.example.service;

public interface ICalculation {
    double rata2(int[] data);
    double nilaiTengah(int[] data);
    double modus(int[] data);
}
