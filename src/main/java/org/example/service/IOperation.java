package org.example.service;

public interface IOperation {
    int readFile(String path);
    int write2(String pathTarget);
    int write(String pathTarget);
}
