package org.example.view;

import java.util.Scanner;

import static org.example.view.MenuUtama.menuUtama;

public class SubMenu {
    public static void subMenu(String hasil){
        MenuUtama menuUtama = new MenuUtama();
        System.out.println("----------------------------------------\n" +
                "Aplikasi Pengolah Nilai Siswa\n" +
                "----------------------------------------\n" +
                hasil +
                "\n0. Exit\n" +
                "1. Kembali ke menu utama\n");
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan pilihan");
        int pilihan = input.nextInt();

        switch (pilihan){
            case 1:
                menuUtama.menuUtama();
                break;
            case 0:
                System.out.println("program selesai");
                break;
            default:
                subMenu("Masukan tidak valid, Pilih sesuai pilihan dibawah");
        }

    }
}
