package org.example.view;

import org.example.service.Operation;

import java.io.File;
import java.util.Scanner;

public class MenuUtama {
    public static void menuUtama(){
        SubMenu subMenu = new SubMenu();
        Scanner input = new Scanner(System.in);
        int[] arrRead;
        Operation op = new Operation();
        String path;
        boolean isExist;
        do {
            System.out.println("Masukkan nama dan path file yang valid :" +
                    "/home/robert/dummy.csv");
            path = input.nextLine();
//            path = "/home/robert/IdeaProjects/Challenge_Chapter2/src/main/resources/dummy.csv";
            isExist = new File(path).exists();
            op.readFile(path);
        }while (!isExist);
        System.out.println("----------------------------------------\n" +
                "Aplikasi Pengolah Nilai Siswa\n" +
                "----------------------------------------\n" +
                "Pilih menu: \n" +
                "1. Generate txt untuk menampilkan modus\n" +
                "2. Generate txt untuk menampilkan nilai rata-rata, median\n" +
                "3. Generate kedua file\n" +
                "0. Exit");
        System.out.println("Masukkan pilihan");
        int pilihan = input.nextInt();

        switch (pilihan){
            case 1:
                System.out.println("Masukkan path target dan nama file(.txt) : " +
                        "\ncontoh : /home/robert/resources/modus.txt");
                String pathTarget = input.next();
                op.write(pathTarget);
                subMenu.subMenu("file telah digenerate pada " + pathTarget);
                break;
            case 2:
                System.out.println("Masukkan path target dan nama file(.txt) : " +
                        "\ncontoh : /home/robert/resources/modus.txt");
                String pathTarget2 = input.next();
                op.write2(pathTarget2);
                subMenu.subMenu("file telah digenerate pada " + pathTarget2);
                break;
            case 3:
                System.out.println("Masukkan path target dan nama file(.txt) : " +
                        "\ncontoh : /home/robert/resources/modus.txt");
                String pathTarget3 = input.next();
                System.out.println("Masukkan path target dan nama file(.txt) : " +
                        "\ncontoh : /home/robert/resources/modus.txt");
                String pathTarget4 = input.next();
                op.write(pathTarget3);
                op.write2(pathTarget4);
                subMenu.subMenu("kedua file telah digenerate pada " + pathTarget3);
                break;
            case 0:
                System.out.println("Program Selesai");
                break;
            default:
                System.out.println("masukan tidak valid");
                menuUtama();
        }
    }
}
